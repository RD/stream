# stream

This is a modified version of stream from Dr. McCalpin, based on
stream-read from:
https://github.com/patinnc/stream_read

You can get the original stream from:
`wget -O ./stream.c https://www.cs.virginia.edu/stream/FTP/Code/stream.c`

This version adds a 'pure read' and 'pure write' bandwidth subtest.

The output looks like:

```text
Function    Best Rate MB/s  Avg time     Min time     Max time
Copy:           40774.7     0.063859     0.051012     0.119653
Scale:          41976.1     0.064889     0.049552     0.168259
Add:            41950.0     0.096691     0.074374     0.151236
Triad:          41936.7     0.098598     0.074398     0.175033
Read:           45078.1     0.038882     0.023071     0.066945
Write:          18357.4     0.065471     0.056653     0.088876
```

The read subtest casts the float array as an integer array and
sums the array. In openmp this is a reduction operation.

You need to make sure the array size is big enough so that it
doesn't reside mostly in L3. Usually this means multigigabyte
array sizes. Stream reports the array size per array and the
total memory used for the arrays. This is not a 'per cpu'
memory size. The arrays are shared.


# usage

Call `make` to build stream compiled with GCC, with default settings
(`stream_gcc`) and with streaming stores enables (`stream_gcc_temporal`).

If you have the AMD Optimizing C/C++ Compiler (AOCC) or Intel oneAPI DPC++/C++ Compiler
available (`aocc` and `icx`, respectively), you can build stream binaries using these
compilers by calling `make aocc` and `make icx`.

For convenience, the `run-benchmark.sh` script builds all three versions, runs the
benchmark and stores the results in seperate files. The `make-plot.py` script can be
used to generate a plot.

