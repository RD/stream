CFLAGS = -O2 -fopenmp -mcmodel=medium -DSTREAM_TYPE=float -DSTREAM_ARRAY_SIZE=260000000 -DNTIMES=30 -march=native

default: gcc

gcc: stream_gcc stream_gcc_temporal

aocc: stream_aocc stream_aocc_temporal

icx: stream_icx stream_icx_temporal

stream_gcc: stream.c
	g++ $^ -o $@ ${CFLAGS} -ffp-contract=fast

stream_aocc: stream.c
	clang $^ -o $@ ${CFLAGS} -ffp-contract=fast

stream_icx: stream.c
	icx $^ -o $@ ${CFLAGS} -qopt-streaming-stores=never

stream_gcc_temporal: stream.c
	g++ $^ -o $@ ${CFLAGS} -ffp-contract=fast -ffloat-store

stream_aocc_temporal: stream.c
	clang $^ -o $@ ${CFLAGS} -ffp-contract=fast -fnt-store

stream_icx_temporal: stream.c
	icx $^ -o $@ ${CFLAGS} -qopt-streaming-stores=always
