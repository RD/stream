#!/usr/bin/python3

import argparse
import os
import regex
from io import StringIO
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

sns.set_theme(style="whitegrid")
sns.set_context("paper", rc={"axes.titlesize": 7, "axes.labelsize": 12})
sns.set(font_scale=1.0)
plt.rcParams["figure.figsize"] = (10, 4)

parser = argparse.ArgumentParser("Parse STREAM output files")
parser.add_argument(
    "-d",
    "--directory",
    help="Directory to read output files from",
    type=str,
    default=".",
)
parser.add_argument(
    "-p",
    "--peak",
    help="Theoretical peak memory bandwidth in GB/s (#channels x GT/s x 8)",
    type=str,
)
parser.add_argument(
    "-o", "--output", help="Output filename", type=str, default="plot.png"
)

args = parser.parse_args()

directory = args.directory
filenames = list(filter(lambda x: ".out" in x, os.listdir(directory)))

df_aio = pd.DataFrame()


def read_df(compiler, data):
    try:
        df = pd.read_csv(
            StringIO(data),
            delim_whitespace=True,
            header=None,
            names=["kernel", "bandwidth", "avg_time", "min_time", "max_time"],
        )
        df["compiler"] = compiler
        df["bandwidth"] /= 1000
        df["kernel"] = df["kernel"].apply(lambda x: x[:-1].lower())
        return df
    except:
        return pd.DataFrame()


for filename in filenames:
    re = r"(\w+)(\-\w+)?(.out)"
    compiler = regex.match(re, filename).group(1)
    lines = open(f"{directory}/{filename}").readlines()
    data = list()
    for line in lines:
        re = r"(\w+):\s+(\d+.\d+)\s+(\d+.\d+)\s+(\d+.\d+)\s+(\d+.\d+)"
        match = regex.match(re, line)
        if match:
            data.append(line)
    df = read_df(compiler, "".join(data))
    df["temporal"] = "temporal" in filename
    df_aio = pd.concat([df_aio, df])

if len(df_aio) == 0:
    raise Exception(
        "No data found, did you specify a directory with at least one '<compiler>.out' file?"
    )

graph = sns.scatterplot(
    data=df_aio, x="kernel", y="bandwidth", hue="compiler", style="temporal", s=200
)
_ = plt.legend(
    markerscale=2, bbox_to_anchor=(1.01, 1), loc="upper left", borderaxespad=0
)

ax = plt.gca()
_ = ax.set_xlabel("Kernel")
_ = ax.set_ylabel("Bandwidth [GB/s]")

if args.peak:
    graph.axhline(eval(args.peak), color="black", ls="--")

plt.savefig(args.output, bbox_inches="tight")
