#!/bin/sh
NUMACTL="numactl --cpunodebind=0"

make -B gcc
${NUMACTL} ./stream_gcc |& tee gcc.out
${NUMACTL} ./stream_gcc_temporal |& tee gcc-temporal.out

make -B aocc
${NUMACTL} ./stream_aocc |& tee aocc.out
${NUMACTL} ./stream_aocc_temporal |& tee aocc-temporal.out

make -B icx
${NUMACTL} ./stream_icx |& tee icx.out
${NUMACTL} ./stream_icx_temporal |& tee icx-temporal.out

